﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Raid.Game_Screen
{
    public abstract class Character
    {
        public abstract Texture2D Texture { get; }
        public Tile Tile { get; set; }

        public int MovementSpeed { get; set; }

        public Rectangle Position => Tile.Position;

        protected Character(int x, int y, BattleMap battleMap)
        {
            Tile = battleMap.GetTile(x, y);
            Tile.Character = this;
        }

        public static void LoadContent(ContentManager content)
        {
            Raider.LoadTexture(content);
        }
    }
}
