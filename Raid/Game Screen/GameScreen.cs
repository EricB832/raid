﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Raid.Game_Screen
{
    public class GameScreen : Screen
    {
        private readonly BattleMap _battleMap;
        private Selector _selector;
        private List<Character> _characters;
        
        public GameScreen(int width, int height) : base(width, height)
        {
            _battleMap = new BattleMap(50, 50);
            _selector = new Selector();
            _characters = new List<Character> {new Raider(3, 3, _battleMap)};
        }

        public override void Update(MouseState mouseState, KeyboardState keyboardState, GameTime gameTime)
        {
            _battleMap.Update(mouseState, keyboardState, gameTime, Camera);
            Camera.Update(mouseState, keyboardState, gameTime, _battleMap);
            switch (mouseState.LeftButton)
            {
                case ButtonState.Pressed:
                    _selector.Selecting = true;
                    break;
                case ButtonState.Released:
                    if (_selector.Selecting)
                    {
                        _battleMap.Highlighted.Click();
                    }
                    _selector.Selecting = false;
                    break;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            _battleMap.Draw(spriteBatch);
            DrawCharacters(spriteBatch);
            _selector.Draw(spriteBatch, _battleMap.Highlighted);
            
        }
        private void DrawCharacters(SpriteBatch spriteBatch)
        {
            foreach (var character in _characters)
            {
                spriteBatch.Draw(character.Texture, character.Position, Color.White);
            }
        }

    }
}
