﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Raid.Game_Screen
{
    public class Camera
    {
        public Vector2 Position;

        public Vector2 TopLeft => new Vector2(Position.X - Width/2f, Position.Y - Height/2f);

        public int ScreenWidth { get; }
        public int ScreenHeight { get; }


        public float Zoom { get; set; }
        public float ScrollSpeed => 0.2f/(Zoom/1.5f);
        public float ZoomSpeed { get; set; }
        public float MinZoom { get; set; }
        public float MaxZoom { get; set; }

        private int _lastScrollValue;

        public Camera(int screenWidth, int screenHeight)
        {
            ScreenWidth = screenWidth;
            ScreenHeight = screenHeight;
            Position = ViewportCenter;
            Zoom = 1;
            MinZoom = 0.33f;
            MaxZoom = 1f;
            ZoomSpeed = 0.0005f;
            _lastScrollValue = 0;
        }

        private float CalculateDelta(GameTime gameTime)
        {
            return ScrollSpeed*gameTime.ElapsedGameTime.Milliseconds;
        }

        public Vector2 ViewportCenter => new Vector2(ScreenWidth * 0.5f, ScreenHeight * 0.5f);

        public Matrix Matrix => Matrix.CreateTranslation(-(int) Position.X, -(int)Position.Y, 0)*
                                Matrix.CreateScale(Zoom, Zoom, 1) *
                                Matrix.CreateTranslation(new Vector3(ViewportCenter, 0));

        public void Update(MouseState mouseState, KeyboardState keyboardState, GameTime gameTime, BattleMap battleMap)
        {
            if (keyboardState.IsKeyDown(Keys.W))
            {
                Position.Y -= CalculateDelta(gameTime);
            }
            if (keyboardState.IsKeyDown(Keys.A))
            {
                Position.X -= CalculateDelta(gameTime);
            }
            if (keyboardState.IsKeyDown(Keys.S))
            {
                Position.Y += CalculateDelta(gameTime);
            }
            if (keyboardState.IsKeyDown(Keys.D))
            {
                Position.X += CalculateDelta(gameTime);
            }

            if (mouseState.ScrollWheelValue != _lastScrollValue)
            {
                HandleZoom(mouseState);
            }

            _lastScrollValue = mouseState.ScrollWheelValue;

            ClampPosition(battleMap);
            
        }

        public int Width => (int) (ScreenWidth / Zoom);
        public int Height => (int) (ScreenHeight / Zoom);

        private void HandleZoom(MouseState mouseState)
        {
            Zoom += (mouseState.ScrollWheelValue - _lastScrollValue) * ZoomSpeed;
            Zoom = Clamp(MinZoom, MaxZoom, Zoom);
        }

        private void ClampPosition(BattleMap battleMap)
        {
            var minX = Width/2 - 10;
            var minY = Height/2 - 10;
            var maxX = battleMap.Width*Tile.Size - Width/2 + 10;
            var maxY = battleMap.Height*Tile.Size - Height/2 + 10;
            Position.X = Clamp(minX, maxX, Position.X);
            Position.Y = Clamp(minY, maxY, Position.Y);
        }

        private static float Clamp(float min, float max, float value)
        {
            if (value < min) return min;
            if (value > max) return max;
            return value;
        }
    }
}
