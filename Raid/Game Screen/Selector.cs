﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Raid.Game_Screen
{
    public class Selector
    {
        private static Texture2D HighlightTexture { get; set; }
        private static Texture2D PressTexture { get; set; }

        public bool Selecting { get; set; }

        public static void LoadContent(ContentManager content)
        {
            HighlightTexture = content.Load<Texture2D>("Tiles/SelectorHighlight");
            PressTexture = content.Load<Texture2D>("Tiles/SelectorPress");
        }

        public void Draw(SpriteBatch spriteBatch, Tile highlighted)
        {
            if (highlighted == null)
            {
                return;
            }
            spriteBatch.Draw(Selecting ? PressTexture : HighlightTexture, highlighted.Position, Color.White);
        }
    }
}
