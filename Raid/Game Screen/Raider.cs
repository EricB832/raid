﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Raid.Game_Screen
{
    public class Raider : Character
    {
        private static Texture2D _texture;
        public override Texture2D Texture => _texture;

        public static void LoadTexture(ContentManager content)
        {
            _texture = content.Load<Texture2D>("Characters/Raider");
        }

        public Raider(int x, int y, BattleMap battleMap) : base(x, y, battleMap)
        {
            MovementSpeed = 4;
        }
    }
}