﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Raid.Game_Screen
{
    public abstract class TileObject
    {
        public abstract Texture2D Texture { get; }

        public static void LoadContent(ContentManager content)
        {
            Tree.LoadTexture(content);
        }
    }
}
