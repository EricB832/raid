﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Raid.Game_Screen
{
    public enum TileType
    {
        Dirt,
        Water
    }

    public class Tile
    {
        private static Dictionary<TileType, Texture2D> _tileTextures; 

        public Point Cell { get; set; }
        public TileType TileType { get; set; }
        public Color Color { get; set; }
        public TileObject Object { get; set; }
        public Character Character { get; set; }

        public const int Size = 64;

        public Texture2D Texture => _tileTextures[TileType];
        public Rectangle Position => new Rectangle(Cell.X * Size, Cell.Y * Size, Size, Size);

        public Tile(int x, int y)
        {
            Cell = new Point(x, y);
            TileType = TileType.Dirt;
            Color = Color.White;

            var roll = Raid.Random.Next(1, 100);
            if (roll > 95)
            {
                Object = new Tree();
            }
        }

        public static void LoadContent(ContentManager content)
        {
            _tileTextures = new Dictionary<TileType, Texture2D> { { TileType.Dirt, content.Load<Texture2D>("Tiles/Dirt") } };
            TileObject.LoadContent(content);
        }

        public void Click()
        {

        }
    }
}
