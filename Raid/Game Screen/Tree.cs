﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Raid.Game_Screen
{
    public class Tree : TileObject
    {
        private static Texture2D _texture;
        public override Texture2D Texture => _texture;

        public static void LoadTexture(ContentManager content)
        {
            _texture = content.Load<Texture2D>("Objects/Tree");
        }
   
    }
}
