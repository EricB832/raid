﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Raid.Game_Screen
{
    public class BattleMap
    {
        private readonly List<List<Tile>> _tiles;

        public int Width { get; set; }
        public int Height { get; set; }
        
        public Tile Highlighted { get; set; }

        public BattleMap(int width, int height)
        {
            Width = width;
            Height = height;
            _tiles = new List<List<Tile>>();
            foreach (var i in Enumerable.Range(0, width))
            {
                _tiles.Add(new List<Tile>());
                foreach (var j in Enumerable.Range(0, height))
                {
                    _tiles[i].Add(new Tile(i, j));
                }
            }
        }

        public void Update(MouseState mouseState, KeyboardState keyboardState, GameTime gameTime, Camera camera)
        {
            Highlighted = GetSelectedTile(mouseState, camera);
        }

        private Tile GetSelectedTile(MouseState mouseState, Camera camera)
        {
            var x = (int) (camera.TopLeft.X + mouseState.X/camera.Zoom) / Tile.Size;
            var y = (int) (camera.TopLeft.Y + mouseState.Y/camera.Zoom) / Tile.Size;
            return GetTile(x, y);
        }

        public Tile GetTile(int x, int y)
        {
            return Contains(x, y) ? _tiles[x][y] : null;
        }

        private bool Contains(int x, int y)
        {
            return (x >= 0 && x < Width && y >= 0 && y < Height);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var tile in _tiles.SelectMany(t => t))
            {
                spriteBatch.Draw(tile.Texture, tile.Position, tile.Color);
                if (tile.Object != null)
                {
                    spriteBatch.Draw(tile.Object.Texture, tile.Position, tile.Color);
                }
            }
        }

    }
}
