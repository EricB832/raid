﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Raid.Game_Screen;
using Raid.Main_Menu;

namespace Raid
{

    public enum ScreenType
    {
        MainMenu,
        Game
    }

    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Raid : Game
    {
        internal GraphicsDeviceManager Graphics;
        internal SpriteBatch SpriteBatch;
        internal Screen Screen;

        public static ScreenType ScreenType { get; set; }
        public static Random Random { get; set; }

        public Raid()
        {
            Graphics = new GraphicsDeviceManager(this);
            Random = new Random();
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            IsMouseVisible = true;
            ScreenType = ScreenType.MainMenu;
            Screen = new MainMenu(Content, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);

            base.Initialize();
        }

        /// <summary>
        /// LoadTexture will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            Selector.LoadContent(Content);
            Tile.LoadContent(Content);
            Character.LoadContent(Content);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            Screen.Update(Mouse.GetState(), Keyboard.GetState(), gameTime);
            HandleScreenChanges();

            base.Update(gameTime);
        }

        private void HandleScreenChanges()
        {
            if (ScreenType == ScreenType.Game && Screen.GetType() == typeof (MainMenu))
            {
                Screen = new GameScreen(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            SpriteBatch.Begin(transformMatrix: Screen.Camera.Matrix);
            Screen.Draw(SpriteBatch);
            SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
