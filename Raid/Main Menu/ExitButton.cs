﻿using System;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Raid.Main_Menu
{
    public class ExitButton : Button
    {

        public ExitButton(ContentManager content, int x, int y) : base(x, y)
        {
            LoadContent(content);
        }

        private void LoadContent(ContentManager content)
        {
            IdleTexture = content.Load<Texture2D>("Buttons/ExitIdle");
            HighlightTexture = content.Load<Texture2D>("Buttons/ExitHighlight");
            PressTexture = content.Load<Texture2D>("Buttons/ExitPress");
        }

        protected override void Execute()
        {
            Environment.Exit(1);
        }

    }
}
