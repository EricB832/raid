﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Raid.Text;

namespace Raid.Main_Menu
{
    public class MainMenu : Screen
    {
        private readonly TitleText _titleText;
        public List<Button> Buttons;

        public MainMenu(ContentManager content, int width, int height) : base(width, height)
        {
            _titleText = new TitleText(content, Width, Height);

            Buttons = new List<Button>
            {
                new StartButton(content, Width/2 - 50, Height - 150),
                new ExitButton(content, Width/2 - 50, Height - 75)
            };
        }

        public override void Update(MouseState mouseState, KeyboardState keyboardState, GameTime gameTime)
        {
            foreach (var button in Buttons)
            {
                button.Update(mouseState);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            _titleText.Draw(spriteBatch);

            foreach (var button in Buttons)
            {
                button.Draw(spriteBatch);
            }
        }
    }
}
