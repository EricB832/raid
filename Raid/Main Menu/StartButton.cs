﻿using System;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Raid.Main_Menu
{
    public class StartButton : Button
    {

        public StartButton(ContentManager content, int x, int y) : base(x, y)
        {
            LoadContent(content);
        }

        private void LoadContent(ContentManager content)
        {
            IdleTexture = content.Load<Texture2D>("Buttons/StartIdle");
            HighlightTexture = content.Load<Texture2D>("Buttons/StartHighlight");
            PressTexture = content.Load<Texture2D>("Buttons/StartPress");
        }

        protected override void Execute()
        {
            Raid.ScreenType = ScreenType.Game;
        }
        
    }
}
