﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Raid.Main_Menu;

namespace Raid
{
    public enum MenuButtonState
    {
        Idle,
        Highlighted,
        Pressed
    }

    public class Button
    {
        protected static int Width = 100;
        protected static int Height = 50;

        protected MenuButtonState MenuButtonState = MenuButtonState.Idle;

        protected Texture2D IdleTexture;
        protected Texture2D HighlightTexture;
        protected Texture2D PressTexture;

        public Rectangle Position;
        
        public Button(int x, int y)
        {
            Position = new Rectangle(x, y, Width, Height);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(GetTexture(), Position, Color.White);
        }

        protected Texture2D GetTexture()
        {
            switch (MenuButtonState)
            {
                default:
                    return IdleTexture;
                case MenuButtonState.Highlighted:
                    return HighlightTexture;
                case MenuButtonState.Pressed:
                    return PressTexture;
            }
        }

        protected virtual void Execute()
        {
            
        }

        public void Update(MouseState mouseState)
        {
            if (!Position.Contains(mouseState.Position))
            {
                MenuButtonState = MenuButtonState.Idle;
            }
            else
            {
                switch (mouseState.LeftButton)
                {
                    case ButtonState.Pressed:
                        MenuButtonState = MenuButtonState.Pressed;
                        break;
                    case ButtonState.Released:
                        if (MenuButtonState == MenuButtonState.Pressed)
                        {
                            Execute();
                        }
                        MenuButtonState = MenuButtonState.Highlighted;
                        break;
                }
            }
        }

    }
}
