﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Raid.Game_Screen;

namespace Raid
{
    public abstract class Screen
    {
        protected int Width;
        protected int Height;
        public Camera Camera { get; }

        public abstract void Update(MouseState mouseState, KeyboardState keyboardState, GameTime gameTime);
        public abstract void Draw(SpriteBatch spriteBatch);

        protected Screen(int width, int height)
        {
            Width = width;
            Height = height;
            Camera = new Camera(width, height);
        }
    }
}
