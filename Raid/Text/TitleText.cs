﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Raid.Text
{
    public class TitleText
    {
        private const string Text = "RAID";

        private readonly SpriteFont _font;
        private readonly Vector2 _position;
        

        public TitleText(ContentManager content, int width, int height)
        {
            _font = content.Load<SpriteFont>("Fonts/Optimus Princeps");
            var stringWidth = _font.MeasureString(Text).X;
            _position = new Vector2(width / 2 - stringWidth / 2, 100);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(_font, Text, _position, Color.Orange);
        }
    }
}
